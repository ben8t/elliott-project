
import pandas as pd 
import numpy as np
import matplotlib.pyplot as plt
from sklearn.neural_network import MLPRegressor
from sklearn.metrics import mean_squared_error
from sklearn.externals import joblib
import plotly as py
import plotly.graph_objs as go

#Loading Model
clf = joblib.load('model1.pkl') 

#Last day data
df = pd.read_csv("lastdata.csv")
data=list(reversed(list(df['Close'])))
x_data = [i for i in range(0,len(data))]
time_data = list(reversed(list(df['Date'])))
test_data=data[0:20]


days = 100
forecast=[]
for i in range(0,days):
	p = clf.predict(test_data)[0]
	test_data.remove(test_data[0])
	test_data.append(p)
	forecast.append(p)

import plotly as py
import plotly.graph_objs as go

forecast = [None for i in range(0,20)] + forecast
print(forecast)
trace1=go.Scatter(
	x=time_data,
	y=data,
	name="3 months data",
	mode = 'lines+markers',
	line=dict(
		color="#4183D7"
		)
)
trace2=go.Scatter(
	x=time_data,
	y=forecast,
	name="forecasting",
	mode = 'lines+markers',
	line=dict(
		color="3FC380"
		)
)



py.offline.plot([trace1,trace2])


