#nn.py

#With scikit-learn

import pandas as pd 
import numpy as np
import matplotlib.pyplot as plt
from sklearn.neural_network import MLPRegressor
from sklearn.metrics import mean_squared_error

#################
#GLOBAL VARIABLES

TRAIN_SIZE = 20
TARGET_SIZE = 1
LAG_SIZE = 1


SPLIT_RATE=0.75
HIDDEN_LAYER_SIZES = (500,250)
BOOL_PLOT = True
BOOL_SAVE_RESULT = False
BOOL_SAVE_MODEL=True
#################


df = pd.read_csv("final_data.csv",sep=',')
df = df.dropna(axis=0,how='any')

df_train = df[0:int(len(df) * SPLIT_RATE)]
from sklearn.utils import shuffle
#df_train= shuffle(df_train)

df_test = df[int(len(df) * SPLIT_RATE):]


print("SPLIT RATE : ",SPLIT_RATE)
print("TRAIN SIZE : ",len(df_train))
print("TEST SIZE : ",len(df_test))
print("NEURAL NETWORK DIMENSION : ",HIDDEN_LAYER_SIZES)

X_train = df_train[df_train.columns[0:TRAIN_SIZE]]
y_train = df_train[df_train.columns[TRAIN_SIZE]]


clf = MLPRegressor(activation='identity',alpha=1e-5,hidden_layer_sizes=HIDDEN_LAYER_SIZES, random_state=1,verbose=True)
clf.fit(X_train, y_train) 
#print(clf)

X_test = df_test[df_test.columns[0:TRAIN_SIZE]]
y_test = df_test[df_test.columns[TRAIN_SIZE]]

R2 = clf.score(X_test, y_test, sample_weight=None)
print("R^2 : ",R2)



predicted = clf.predict(X_test)

MSE = mean_squared_error(y_test, predicted)
print("MSE : ",MSE)

if(BOOL_PLOT):
	import plotly as py
	import plotly.graph_objs as go

	trace1=go.Scatter(
		x=[i for i in range(0,150)],
		y=y_test[:150],
		name="Real data",
		mode = 'line',
		line=dict(
			color="#4183D7"
			)
	)

	trace2=go.Scatter(
		x=[i for i in range(0,150)],
		y=predicted[:150],
		mode ='line',
		name="Predicted data",
		line=dict(
			color="#1BBC9B"
			)
	)

	py.offline.plot([trace1,trace2])


#Write result in result.csv
if BOOL_SAVE_RESULT:
	import csv
	try:
		result = [TRAIN_SIZE,TARGET_SIZE,LAG_SIZE,SPLIT_RATE,HIDDEN_LAYER_SIZES[0],HIDDEN_LAYER_SIZES[1],MSE,R2]
	except:
		result = [TRAIN_SIZE,TARGET_SIZE,LAG_SIZE,SPLIT_RATE,HIDDEN_LAYER_SIZES,"none",MSE,R2]
	myfile = open("result.csv", 'a')
	wr = csv.writer(myfile)
	wr.writerow(result)
	print("RESULTS SAVE : OK")

if BOOL_SAVE_MODEL:
	#model_name = "model_"+str(TRAIN_SIZE)+"_"+str(TARGET_SIZE)+"_"+str(LAG_SIZE)+"_"+str(SPLIT_RATE)+"_"+str(HIDDEN_LAYER_SIZES)+"_"+".pkl"
	model_name="model1.pkl"
	from sklearn.externals import joblib
	joblib.dump(clf, model_name) 


