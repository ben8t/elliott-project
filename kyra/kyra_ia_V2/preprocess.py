#explode.py

import numpy as np
import pandas as pd 
import matplotlib.pyplot as plt

#################
#GLOBAL VARIABLES

#Sampling rate
DT = 30

#Sampling size of prediction
DTP = 1

#################

#Read dataset
df = pd.read_csv("table.csv")
#print(df[:4])

#Normalize data
df['Close']=(df['Close']-df['Close'].min())/(df['Close'].max()-df['Close'].min())

print("Sample size : ",len(df['Close']))
print("Sampling rate : ",DT)

f = lambda A, n=DT: [A[i:i+n] for i in range(0, len(A), n)]
sample=f(df['Close'])


print("Number of sample : ",len(sample))

#Split sample in two parts (one for training and another to forecasting)
v1 =[]
v2 = []
for s in sample:
	v1.append(list(s[:DT-DTP]))
	v2.append(list(s[DT-DTP:]))


#Building dataframe for an easiest output
inputData = pd.DataFrame(v1, columns=["x"+str(x) for x in range(0,DT-DTP)])
outputData = pd.DataFrame(v2, columns=["x"+str(x) for x in range(DT-DTP,DT)])
final_data = pd.concat([inputData,outputData],axis=1)

#Shuffling dataframe
from sklearn.utils import shuffle
final_data = shuffle(final_data)

#Ploting one example
plt.plot(range(0,DT-DTP),final_data.iloc[0][:DT-DTP],'-r',range(DT-DTP,DT),final_data.iloc[0][DT-DTP:],'-b')
plt.show()

#Save data to 'final_data.csv'
final_data.to_csv('final_data.csv',index=False)


