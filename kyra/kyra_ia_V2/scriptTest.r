#load csv eurdol
#var : dat
load("~/Documents/Informatique/HISTDATA_COM_MT_EURUSD_M1201612/dat.RData")


#Visualization on hour

	#get first hour (0h-1h)
	hour = dat[1:60,]

	part1=hour$V3[1:30]
	part2=hour$V3[31:60]
	x=1:60
	x1 = 1:30
	x2 = 31:60

	m1 = lm(part1~x1 + I(x1^2) + I(x1^3) + I(x1^4) + I(x1^5))
	m2 = lm(part2~x2 + I(x2^2) + I(x2^3) + I(x2^4) + I(x2^5))


	#print(m1$coef)
	#print(m2$coef)
	par(mfrow=c(3,1))
	plot(hour$V3)
	plot(part1)
	lines(predict(m1),col='red',lwd=3)
	plot(part2)
	lines(predict(m2),col='green',lwd=3)


	#Other test (between 8h-9h)
	hour2 = dat[481:540,]

	part1=hour2$V3[1:30]
	part2=hour2$V3[31:60]
	x=1:60
	x1 = 1:30
	x2 = 31:60

	m1 = lm(part1~x1 + I(x1^2) + I(x1^3) + I(x1^4) + I(x1^5))
	m2 = lm(part2~x2 + I(x2^2) + I(x2^3) + I(x2^4) + I(x2^5))


	#print(m1$coef)
	#print(m2$coef)
	par(mfrow=c(3,1))
	plot(hour2$V3)
	plot(part1)
	lines(predict(m1),col='red',lwd=3)
	plot(part2)
	lines(predict(m2),col='green',lwd=3)


#Visualization on day
	#first day
	day = "2016.12.01"
	dayOne = dat[dat$V1==day,]$V3
	dayOneP1 = dayOne[1:(length(dayOne)/2)]
	dayOneP2 = dayOne[(length(dayOne)/2 + 1 ): length(dayOne)]
	x1 = 1:(length(dayOne)/2)
	x2 = (length(dayOne)/2 + 1 ): length(dayOne)

	m1 = lm(dayOneP1~ x1 + I(x1^2) + I(x1^3) + I(x1^4) + I(x1^5))
	m2 = lm(dayOneP2~x2 + I(x2^2) + I(x2^3) + I(x2^4) + I(x2^5))

	print(m1$coef)
	print(m2$coef)
	par(mfrow=c(3,1))
	plot(dayOne,main=day,ylab="EURDOL",xlab="Time")
	plot(dayOneP1,main="First part of the day",ylab="EURDOL",xlab="Time")
	lines(predict(m1),col='red',lwd=3)
	plot(dayOneP2,main="Second part of the day",ylab="EURDOL",xlab="Time")
	lines(predict(m2),col='green',lwd=3)

evalCoef <- function(a,b,c,d,e,f,x){
return ((a)*(x^5) + (b)*(x^4) + (c)*(x^3) + (d)*(x^2) + (e)*x + f)
}

#TEST 1
cat("\nNEURONAL NETWORK TEST #1 \n------------------------------------------------------\n")
cat("PARAMS : \n") 
cat ("Input : 2.906243e-15,-4.965515e-12,2.981038e-09,-7.531691e-07,7.604081e-05,1.059466e+00 \n")
cat("Expected output : -6.418908e-16,3.530745e-12,-7.640167e-09,8.116066e-06,-4.218455e-03,1.919631e+00 \n")
cat("Neuronal network output : 0.00628079,-0.01041529,-0.02164578,-0.00412202,-0.01250392,1.00825885 \n")
par(mfrow=c(2,3))
plot(0:700,evalCoef(2.906243e-15,-4.965515e-12,2.981038e-09,-7.531691e-07,7.604081e-05,1.059466e+00,0:700),col="red",main="First part of the day",ylab="EURDOL",xlab="Time")
plot(700:1400,evalCoef(-6.418908e-16,3.530745e-12,-7.640167e-09,8.116066e-06,-4.218455e-03,1.919631e+00,700:1400),main="Second part of the day - TARGET",ylab="EURDOL",xlab="Time",col="green")
plot(700:1400,evalCoef(0.00628079,-0.01041529,-0.02164578,-0.00412202,-0.01250392,1.00825885,700:1400),col="blue",main="Neuronal Network #1",ylab="Trend",xlab="Time")
plot(700:1400,evalCoef(0.02522737,0.01015902,-0.00486322,-0.02586735,0.01995683,0.75860015,700:1400),col="blue",main="Neuronal Network #2",ylab="Trend",xlab="Time")
plot(700:1400,evalCoef(0.01305648,0.00346346,-0.00540802,0.02452836,-0.01331543,1.19622638,700:1400),col="blue",main="Neuronal Network #3",ylab="Trend",xlab="Time")
plot(700:1400,evalCoef(0.00667353,-0.00117365,0.02851415,0.00935819,0.03204386,0.97245705,700:1400),col="blue",main="Neuronal Network #4",ylab="Trend",xlab="Time")



