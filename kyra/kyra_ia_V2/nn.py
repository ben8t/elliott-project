#nn.py

'''
With Pybrain
import pandas as pd 
import numpy as np
import matplotlib.pyplot as plt
df = pd.read_csv("final_data.csv")

from pybrain.datasets import SupervisedDataSet
from pybrain.utilities import percentError
from pybrain.tools.shortcuts import buildNetwork
from pybrain.supervised.trainers import BackpropTrainer

#################
#GLOBAL VARIABLES

#Sampling rate
DT = 10

#Sampling size of prediction
DTP = 1

#################

print(df[:4])
#Building supervised dataset for pybrain from "final_data.csv"
nn_data = SupervisedDataSet(DT-DTP,DTP)


for i in range(df.shape[0]):
	inp = df.iloc[i][0:DT-DTP]
	out = df.iloc[i][DT-DTP:DT]
	nn_data.addSample(inp,out)
	
tstdata, trndata = nn_data.splitWithProportion( 0.25 )

print("Number of training patterns: ", len(trndata))
print("Input and output dimensions: ", trndata.indim, trndata.outdim)
print("First sample (input, target):")
print(trndata['input'][0], trndata['target'][0])

fnn = buildNetwork(trndata.indim,5,trndata.outdim)


# print("PARAMS : ",fnn.params)
# new_params = np.array([1.0]*97)
# fnn._setParameters(new_params)
# print("PARAMS : ",fnn.params)


trainer = BackpropTrainer( fnn, dataset=trndata, momentum=0.0, verbose=True, weightdecay=0.0)

error= trainer.trainUntilConvergence(maxEpochs = 10)



#Get distance between output and expected output
# from scipy.spatial import distance
# for input, expectedOutput in tstdata:
#     output = fnn.activate(input)
#     dst = distance.euclidean(output,expectedOutput)
#     print(dst)
    
print("First sample testdata(input, expectedOutput,output):")
print(tstdata['input'][0]) 
print(tstdata['target'][0])
print(fnn.activate(tstdata['input'][0]))


#RMSE (Root Mean Square Error)
#from pybrain.tools.validation import ModuleValidator
#mse = ModuleValidator.MSE(fnn,tstdata)
#print("RMSE : ",np.sqrt(mse))



plt.plot(range(0,DT-DTP),tstdata['input'][0],'-b',range(DT-DTP,DT),tstdata['target'][0],'-go',range(DT-DTP,DT),fnn.activate(tstdata['input'][0]),'-r^')
plt.show()


plt.plot(error[0])
plt.show()

'''

#With scikit-learn

import pandas as pd 
import numpy as np
import matplotlib.pyplot as plt
from sklearn.neural_network import MLPRegressor
from sklearn.metrics import mean_squared_error

#################
#GLOBAL VARIABLES


#Sampling rate
DT = 30

#Sampling size of prediction
DTP = 1

#################

train_range = range(0,DT-DTP)
test_range = range(DT-DTP,DT)

df = pd.read_csv("final_data.csv",sep=',')
df = df.dropna(axis=0,how='any')
df_train = df[:100]
df_test = df[100:]


X_train = df_train[df_train.columns[0:DT-DTP]]
y_train = df_train[df_train.columns[DT-DTP:DT]]
clf = MLPRegressor(activation='logistic',alpha=1e-5,hidden_layer_sizes=(10), random_state=1,verbose=True)
clf.fit(X_train, y_train) 
print(clf)

X_test = df_test[df_test.columns[0:DT-DTP]]
y_test = df_test[df_test.columns[DT-DTP:DT]]
#print("R^2 : ",clf.score(X_test, y_test, sample_weight=None))



predicted = clf.predict(X_test)

print("RMSE : ",mean_squared_error(y_test, predicted))

print(len(y_test))
plt.plot(range(0,150),y_test[:150],color='blue')
plt.plot(range(0,150),predicted[:150],color='green')
plt.show()

# print(clf.n_outputs_)
# #new_predict = [x for y in clf.predict(df_test.iloc[0,0:DT-DTP]) for x in y]
# new_predict = clf.predict(df_test.iloc[0,0:DT-DTP])
# print(list(df_test.iloc[0,DT-DTP:DT]))
# print(new_predict)

# plt.plot(train_range,list(df_test.iloc[0,0:DT-DTP]),'-b',test_range,list(df_test.iloc[0,DT-DTP:DT]),'-go',test_range,new_predict,'-r^')
# plt.show()


































