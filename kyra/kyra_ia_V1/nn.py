#nn.py


import pandas as pd 
import numpy as np
import matplotlib.pyplot as plt
df = pd.read_csv("final_data.csv")
#Normalize data
df = (df - df.mean())/df.std()

print('MIN :',df.min())
print('MAX :',df.max())
print('MEAN :',df.mean())
print('STANDARD DEVIATION:',df.std())

from pybrain.datasets import SupervisedDataSet
from pybrain.utilities import percentError
from pybrain.tools.shortcuts import buildNetwork
from pybrain.supervised.trainers import BackpropTrainer

#Building supervised dataset for pybrain from "final_data.csv"
nn_data = SupervisedDataSet(4,4)


for i in range(df.shape[0]):
	inp = df.iloc[i][0:4]
	out = df.iloc[i][4:8]
	nn_data.addSample(inp,out)
	
tstdata, trndata = nn_data.splitWithProportion( 0.25 )

print("Number of training patterns: ", len(trndata))
print("Input and output dimensions: ", trndata.indim, trndata.outdim)
print("First sample (input, target):")
print(trndata['input'][0], trndata['target'][0])

fnn = buildNetwork(trndata.indim,5,trndata.outdim)


# print("PARAMS : ",fnn.params)
# new_params = np.array([1.0]*97)
# fnn._setParameters(new_params)
# print("PARAMS : ",fnn.params)


trainer = BackpropTrainer( fnn, dataset=trndata, momentum=0.1, verbose=True, weightdecay=0.01)

error= trainer.trainUntilConvergence(maxEpochs = 50)



#Get distance between output and expected output
# from scipy.spatial import distance
# for input, expectedOutput in tstdata:
#     output = fnn.activate(input)
#     dst = distance.euclidean(output,expectedOutput)
#     print(dst)
    
print("First sample testdata(input, expectedOutput,output):")
print(tstdata['input'][0]) 
print(tstdata['target'][0])
print(fnn.activate(tstdata['input'][0]))


#RMSE (Root Mean Square Error)
from pybrain.tools.validation import ModuleValidator
mse = ModuleValidator.MSE(fnn,tstdata)
print("RMSE : ",np.sqrt(mse))



#PLOTING TEST
#First sample
x1=[i for i in range(0,700)]
x2=[i for i in range(700,1400)]
p1=tstdata['input'][0]
p2=tstdata['target'][0]
p3=fnn.activate(tstdata['input'][0])
y1=np.polyval(p1,x1)
y2=np.polyval(p2,x2)
y3=np.polyval(p3,x2)
plt.plot(x1,y1,'r--',x2,y2,'b--',x2,y3,'g--')
plt.show()


plt.plot(error[0])
plt.show()




