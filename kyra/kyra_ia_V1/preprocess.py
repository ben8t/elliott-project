#explode.py

import numpy as np
import pandas as pd 

df = pd.read_csv("DAT_MT_EURUSD_M1_2016.csv")
print("\n DATASET SAMPLE")
print(df[:4])

#Normalize data between 0 and 1
df['V1'] = (df['V1']-df['V1'].min())/(df['V1'].max()-df['V1'].min())
print(df[:4])

#print(df['DAY'].unique())
#print(df[df['DAY']=='2016.12.01'])


#Transforming to dictionary, each date is a key, values are couple where first element
# is a list of values from first part of the day and the second element is
# a list of values from the second part of the day
listTmp =[]
for day in df['DAY'].unique():
	values = df[df['DAY']==day]['V1']
	listSep = [values[0:len(values)//2],values[(len(values)//2)+1:len(values)]]
	listTmp.append([day,listSep])

dataDict ={day:listValue for day,listValue in listTmp}

#Regular dictonnary aren't sorted
import collections as col
dataDict = col.OrderedDict(sorted(dataDict.items()))

#Display length for both input and output
#for key, value in dataDict.items():
#	print(len(value[0])," - ",len(value[1]), " - ",len(value[0])-len(value[1]))


#Test with first day (fit polynomial regression and plotting)
# part1 = dataDict["2016.12.01"][0]

# x= range(0,len(part1))
# y = part1
# z = np.polyfit(x, y, 5)
# print(z)
# p = np.poly1d(z)

# import matplotlib.pyplot as plt
# xp = np.linspace(0,len(part1) ,len(part1))
# _ = plt.plot(x, y, '.', xp, p(xp), '-')
# plt.show()

#Polynomial regression
#Choose degree of polynome
deg = 3
poly_coef_p1 = []
poly_coef_p2 = []
for v1,v2 in dataDict.values():
	x1 = range(0,len(v1))
	x2 = range(len(v2),2*len(v2))
	z1 = np.polyfit(x1,v1, deg)
	z2 = np.polyfit(x2,v2, deg)
	poly_coef_p1.append(z1)
	poly_coef_p2.append(z2)

#Names for the coefficient
import string
alphabet = list(string.ascii_lowercase)

listCoefNames = alphabet[:deg+1]

#Building dataframe for an easiest output
coef_df_1 = pd.DataFrame(poly_coef_p1, columns=[c+"1" for c in listCoefNames])
coef_df_2 = pd.DataFrame(poly_coef_p2, columns=[c+"2" for c in listCoefNames])

print("\n COEFFICIENT DATA")
print(coef_df_1)
print(coef_df_2)

#Concat two DataFrame to have a1,b1,c1,...,f1,a2,b2,...,f2
final_data = pd.concat([coef_df_1,coef_df_2],axis=1)

#Save data to 'final_data.csv'
final_data.to_csv('final_data.csv',index=False)






