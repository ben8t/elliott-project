#explode.py

import numpy as np
import pandas as pd 
import matplotlib.pyplot as plt

#################
#GLOBAL VARIABLES

TRAIN_SIZE = 20
TARGET_SIZE = 5
LAG_SIZE = 25

#################

#Read dataset
df = pd.read_csv("table2.csv")
#print(df[:4])

from sklearn import preprocessing
#scale_data = preprocessing.scale(df['Close'])
scale_data = df['Close']

print("Data Length : ",len(df['Close']))
print("TRAIN SIZE : ",TRAIN_SIZE)
print("TARGET_SIZE : ",TARGET_SIZE)


def splitFromRow(data,train,lag,target):
	v1=[]
	v2=[]
	for i in range(0,len(data),LAG_SIZE):
		try:
			x=np.array(data[i:i+train])
			y=np.array(data[i+train:i+train+target])
		except:
			break
		v1.append(x)
		v2.append(y)
	return v1,v2


x_i,y_i = splitFromRow(scale_data,TRAIN_SIZE,LAG_SIZE,TARGET_SIZE) 

#Building dataframe for an easiest output
inputData = pd.DataFrame(x_i, columns=["x"+str(x) for x in range(1,TRAIN_SIZE+1)])
outputData = pd.DataFrame(y_i, columns=["x"+str(x) for x in range(TRAIN_SIZE+1,TRAIN_SIZE+TARGET_SIZE+1)])
final_data = pd.concat([inputData,outputData],axis=1)


#Shuffling dataframe
from sklearn.utils import shuffle
#final_data = shuffle(final_data)


#Save data to 'final_data.csv'
final_data.to_csv('final_data.csv',index=False)



