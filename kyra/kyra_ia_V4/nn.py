#nn.py

#With scikit-learn

import pandas as pd 
import numpy as np
import matplotlib.pyplot as plt
from sklearn.neural_network import MLPRegressor
from sklearn.metrics import mean_squared_error

#################
#GLOBAL VARIABLES

TRAIN_SIZE = 20
TARGET_SIZE = 5
LAG_SIZE = 25


SPLIT_RATE=0.75
HIDDEN_LAYER_SIZES = (500,250)
BOOL_PLOT = True
BOOL_SAVE_RESULT = False
BOOL_SAVE_MODEL=False
#################


df = pd.read_csv("final_data.csv",sep=',')
df = df.dropna(axis=0,how='any')

df_train = df[0:int(len(df) * SPLIT_RATE)]
from sklearn.utils import shuffle
#df_train= shuffle(df_train)

df_test = df[int(len(df) * SPLIT_RATE):]


print("SPLIT RATE : ",SPLIT_RATE)
print("TRAIN SIZE : ",len(df_train))
print("TEST SIZE : ",len(df_test))
print("NEURAL NETWORK DIMENSION : ",HIDDEN_LAYER_SIZES)

X_train = df_train[df_train.columns[0:TRAIN_SIZE]]
y_train = df_train[df_train.columns[TRAIN_SIZE:TRAIN_SIZE+TARGET_SIZE]]

clf = MLPRegressor(activation='identity',alpha=1e-5,hidden_layer_sizes=HIDDEN_LAYER_SIZES, random_state=1,verbose=True)
clf.fit(X_train, y_train) 
#print(clf)

X_test = df_test[df_test.columns[0:TRAIN_SIZE]]
y_test = df_test[df_test.columns[TRAIN_SIZE:TRAIN_SIZE+TARGET_SIZE]]

R2 = clf.score(X_test, y_test, sample_weight=None)
print("R^2 : ",R2)


predicted = clf.predict(X_test)


MSE = mean_squared_error(y_test, predicted)
print("MSE : ",MSE)

print(predicted[0])
print(y_test.iloc[0])

# import plotly as py
# import plotly.graph_objs as go

# trace1=go.Scatter(
# 	x=[i for i in range(0,TARGET_SIZE)],
# 	y=y_test.iloc[0],
# 	name="Real data",
# 	mode = 'line',
# 	line=dict(
# 		color="#4183D7"
# 		)
# )

# trace2=go.Scatter(
# 	x=[i for i in range(0,TARGET_SIZE)],
# 	y=predicted[0],
# 	mode ='line',
# 	name="Predicted data",
# 	line=dict(
# 		color="#1BBC9B"
# 		)
# )

# py.offline.plot([trace1,trace2])
# print(predicted)
# print(predicted.flatten())
# print(y_test.values.flatten())


if(BOOL_PLOT):
	import plotly as py
	import plotly.graph_objs as go

	pred = predicted.flatten()
	#Color split each by TARGET_SIZE
	t = [True]*TARGET_SIZE
	f = [False]*TARGET_SIZE
	tf=list()
	for i in range(0,len(pred)):
		tf = tf+t+f
	pred1 = [pred[i] if tf[i]==True else None for i in range(0,len(pred))]
	pred2 = [pred[i] if tf[i]==False else None for i in range(0,len(pred))]
	trace1=go.Scatter(
		x=[i for i in range(0,len(df_test)*TARGET_SIZE)],
		y=y_test.values.flatten(),
		name="Real data",
		mode = 'line',
		line=dict(
			color="#4183D7"
			)
	)

	trace2=go.Scatter(
		x=[i for i in range(0,len(df_test)*TARGET_SIZE)],
		y=pred,
		name="Predicted data",
		mode = 'line',
		line=dict(
			color="#BDC3C7"
			)
	)

	trace3=go.Scatter(
		x=[i for i in range(0,len(df_test)*TARGET_SIZE)],
		y=pred1,
		mode ='line',
		name="Predicted data",
		line=dict(
			color="#F89406"
			)
	)

	trace4=go.Scatter(
		x=[i for i in range(0,len(df_test)*TARGET_SIZE)],
		y=pred2,
		mode ='line',
		name="Predicted data",
		line=dict(
			color="#3FC380"
			)
	)

	py.offline.plot([trace1,trace2,trace3,trace4])


#Write result in result.csv
if BOOL_SAVE_RESULT:
	import csv
	try:
		result = [TRAIN_SIZE,TARGET_SIZE,LAG_SIZE,SPLIT_RATE,HIDDEN_LAYER_SIZES[0],HIDDEN_LAYER_SIZES[1],MSE,R2]
	except:
		result = [TRAIN_SIZE,TARGET_SIZE,LAG_SIZE,SPLIT_RATE,HIDDEN_LAYER_SIZES,"none",MSE,R2]
	myfile = open("result.csv", 'a')
	wr = csv.writer(myfile)
	wr.writerow(result)
	print("RESULTS SAVE : OK")

if BOOL_SAVE_MODEL:
	#model_name = "model_"+str(TRAIN_SIZE)+"_"+str(TARGET_SIZE)+"_"+str(LAG_SIZE)+"_"+str(SPLIT_RATE)+"_"+str(HIDDEN_LAYER_SIZES)+"_"+".pkl"
	model_name="model1.pkl"
	from sklearn.externals import joblib
	joblib.dump(clf, model_name) 


